package com.sts.demo.service;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static com.ninja_squad.dbsetup.Operations.sequenceOf;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.operation.Operation;
import com.sts.demo.domain.User;
import com.sts.demo.util.CommonOperations;


public class UserServiceIntTest extends BaseServiceIntTest {

	@Autowired
	private UserService userService;
	
	@Before
    public void setUp() throws Exception {
		Operation operation =
	            sequenceOf(
	                CommonOperations.DELETE_ALL,
	                insertInto("User")
	                	.withDefaultValue("password", "admin")
	                	.withDefaultValue("created_by", "1")
	                	.withDefaultValue("created_date", new Date())
	                    .columns("id", "first_name", "last_name", "email")
	                    .values(1L, "Jack", "Sparrow", "jack.sparrow@gmail.com")
	                    .values(2L, "William", "Turner", "william.turner@gmail.com")
	                    .build());
		
        DbSetup dbSetup = new DbSetup(destination, operation);
		getDbSetupTracker().launchIfNecessary(dbSetup);
	}
	
	@Test
	public void findAllUsers_ShouldReturnListOfUsers() {
		// when
		List<User> userList = userService.findAllUsers();
		
		// then
		assertThat(userList).hasSize(2);
		assertThat(userList).extracting("firstname").contains("Jack", "William");
	}
	
	@Test
	public void deleteUser_ShouldDeleteGivenUser() {
		// when
		userService.deleteUser(1L);
		
	}
	
	@Test
	public void findUserByEmail_ShouldReturnGivenUser() {
		// when
		User user = userService.findUserByEmail("jack.sparrow@gmail.com");
		
		// then
		assertThat(user).isNotNull();
		assertThat(user.getId()).isEqualTo(1L);
		assertThat(user.getEmail()).isEqualTo("jack.sparrow@gmail.com");
	}

	@After
    public void teardown() throws Exception {
		Operation operation = sequenceOf(CommonOperations.DELETE_ALL);
        DbSetup dbSetup = new DbSetup(destination, operation);
        dbSetup.launch();
	}	
}
