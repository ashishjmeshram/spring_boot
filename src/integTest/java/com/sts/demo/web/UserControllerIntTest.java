package com.sts.demo.web;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static com.ninja_squad.dbsetup.Operations.sequenceOf;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.operation.Operation;
import com.sts.demo.config.WithMockUser;
import com.sts.demo.util.CommonOperations;
import com.sts.demo.util.WebUtil;

@WithMockUser
public class UserControllerIntTest extends BaseControllerIntTest {

	@Autowired
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
    public void setUpData() throws Exception {
		Operation operation =
	            sequenceOf(
	                CommonOperations.DELETE_ALL,
	                insertInto("User")
	                	.withDefaultValue("password", "XEKENxFpqFkSiazPPKRXOahjCFwFRa7dD0CD0sUAnUXk2evHW5/h5nP6RK02sh9V")
	                	.withDefaultValue("created_by", "1")
	                	.withDefaultValue("created_date", new Date())
	                    .columns("id", "first_name", "last_name", "email")
	                    .values(1L, "Jack", "Sparrow", "jack.sparrow@gmail.com")
	                    .values(2L, "William", "Turner", "william.turner@gmail.com")
	                    .build());
		
        DbSetup dbSetup = new DbSetup(destination, operation);
		getDbSetupTracker().launchIfNecessary(dbSetup);
	}

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
	}

	@Test
    public void userList_shouldAddUserListToModelAndRenderUserListView() throws Exception {
		mockMvc.perform(get("/user?operation=userList"))
                .andExpect(status().isOk())
                .andExpect(view().name("userList"))
                .andExpect(model().attribute("userList", hasSize(2)))
           //     .andExpect(forwardedUrl("/WEB-INF/jsps/user/userList.jsp"))
                .andExpect(model().attribute("userList", hasItem(
                        allOf(
                                hasProperty("id", is(1L)),
                                hasProperty("firstname", is("Jack")),
                                hasProperty("lastname", is("Sparrow"))
                        )
                )))
                .andExpect(model().attribute("userList", hasItem(
                        allOf(
                                hasProperty("id", is(2L)),
                                hasProperty("firstname", is("William")),
                                hasProperty("lastname", is("Turner"))
                        )
                )));
    }
		
	@Test
    public void deleteUser_shouldDeleteUserAndRenderUserListView() throws Exception {
        String expectedRedirectViewPath = WebUtil.createRedirectViewPath("/user?operation=userList");
		mockMvc.perform(get("/user?operation=deleteUser&userId=1"))
                .andExpect(status().isFound())
                .andExpect(view().name(expectedRedirectViewPath));
    }
	
	@Test
    public void editUser_shouldAddUserToModelAndRenderUserFormView() throws Exception {
		mockMvc.perform(get("/user?operation=editUser&userId=1"))
                .andExpect(status().isOk())
                .andExpect(view().name("userForm"))
                .andExpect(model().attribute("user", hasProperty("id", is(1L))))
                .andExpect(model().attribute("user", hasProperty("firstname", is("Jack"))))
                .andExpect(model().attribute("user", hasProperty("lastname", is("Sparrow"))));
    }
	
	@Test
    public void saveUser_shouldUpdateExistingUserAndRenderUserListView() throws Exception {
        String expectedRedirectViewPath = WebUtil.createRedirectViewPath("/user?operation=userList");
        mockMvc.perform(post("/user?operation=saveUser")
                .with(csrf())
        		.contentType(MediaType.APPLICATION_FORM_URLENCODED)
        		.param("id", "1")
        		.param("firstname", "jack")
                .param("lastname", "sparrow")
        		.param("email", "jack.sparrow@gmail.com")
        		.param("password", "admin"))
                .andExpect(status().isFound())
                .andExpect(view().name(expectedRedirectViewPath));
    }

	@Test
    public void saveUser_shouldCreateNewUserAndRenderUserListView() throws Exception {
        String expectedRedirectViewPath = WebUtil.createRedirectViewPath("/user?operation=userList");
        mockMvc.perform(post("/user?operation=saveUser")
                .with(csrf())
        		.contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("firstname", "jack new")
                .param("lastname", "sparrow new")
        		.param("email", "jack.sparrow@gmail.com")
        		.param("password", "admin"))
                .andExpect(status().isFound())
                .andExpect(view().name(expectedRedirectViewPath));
    }

	@Test
    public void newUser_shouldRenderUserFormView() throws Exception {
        mockMvc.perform(get("/user?operation=newUser"))
                .andExpect(status().isOk())
                .andExpect(view().name("userForm"));
    }
}
