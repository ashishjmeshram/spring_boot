package com.sts.demo.web;

import com.sts.demo.Application;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.destination.Destination;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest("server.port:0")
@Transactional
public abstract class BaseControllerIntTest {
	
	@Autowired
    public Destination destination;
	
	private static final DbSetupTracker dbSetupTracker = new DbSetupTracker();

    public static DbSetupTracker getDbSetupTracker() {
        return dbSetupTracker;
    }
}
