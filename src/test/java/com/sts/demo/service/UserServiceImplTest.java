package com.sts.demo.service;

import static com.natpryce.makeiteasy.MakeItEasy.a;
import static com.natpryce.makeiteasy.MakeItEasy.make;
import static com.natpryce.makeiteasy.MakeItEasy.with;
import static com.sts.demo.builder.UserMaker.User;
import static com.sts.demo.builder.UserMaker.id;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import com.sts.demo.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.sts.demo.domain.User;
import com.sts.demo.util.PasswordUtil;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

	@Mock
	private UserRepository userRepository;

	@Mock
	private PasswordUtil passwordUtil;

	@InjectMocks
	private UserService userService = new UserServiceImpl();

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void saveUser_newUserEntry_shouldSaveUser() {
		User newUser = make(a(User, with(id, 0L)));
		
		when(passwordUtil.encryptUserPassword(newUser.getPassword())).thenReturn(newUser.getPassword());

		long loggedInUserId = 2L;

		userService.saveUser(newUser, loggedInUserId);

		ArgumentCaptor<User> toDoArgument = ArgumentCaptor.forClass(User.class);

		verify(userRepository, times(1)).save(toDoArgument.capture());
		verify(passwordUtil, times(1)).encryptUserPassword(newUser.getPassword());

		verifyNoMoreInteractions(userRepository);
		verifyNoMoreInteractions(passwordUtil);

		User model = toDoArgument.getValue();

		assertThat(model).isEqualTo(newUser);
	}

	@Test
	public void deleteUser_userEntryFound_shouldDeleteFoundUserEntry() {
		User userToDelete = make(a(User));
		when(userRepository.findOne(userToDelete.getId())).thenReturn(userToDelete);

		userService.deleteUser(userToDelete.getId());

		verify(userRepository).findOne(userToDelete.getId());
		verify(userRepository).delete(userToDelete);
		verifyNoMoreInteractions(userRepository);
	}

	@Test
	public void findAllUsers_userEntriesFound_shouldReturnListOfAllUsers() {
		User user = make(a(User));
		List<User> expectedUserList = Arrays.asList(user);
		when(userRepository.findAll()).thenReturn(expectedUserList);

		List<User> actualUserList = userService.findAllUsers();

		verify(userRepository).findAll();
		verifyNoMoreInteractions(userRepository);

		assertThat(actualUserList).hasSize(1);
		assertThat(actualUserList).isEqualTo(expectedUserList);
		assertThat(actualUserList).contains(user);
	}

	@Test
	public void findUserById_userEntryFound_shouldReturnFoundUserEntry() {
		User expectedUser = make(a(User));
		when(userRepository.findOne(expectedUser.getId())).thenReturn(expectedUser);

		User actualUser = userService.findUserById(expectedUser.getId());

		verify(userRepository).findOne(expectedUser.getId());
		verifyNoMoreInteractions(userRepository);
		
		assertThat(actualUser).isEqualTo(expectedUser);
	}

	@Test
	public void findUserByEmail_userEntryFound_shouldReturnFoundUserEntry() {
		User expectedUser = make(a(User));
		when(userRepository.findByEmail(expectedUser.getEmail())).thenReturn(expectedUser);

		User actualUser = userService.findUserByEmail(expectedUser.getEmail());

		verify(userRepository).findByEmail(expectedUser.getEmail());
		verifyNoMoreInteractions(userRepository);
		
		assertThat(actualUser).isEqualTo(expectedUser);
	}

}
