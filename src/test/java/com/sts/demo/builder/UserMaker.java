package com.sts.demo.builder;

import com.natpryce.makeiteasy.Instantiator;
import com.natpryce.makeiteasy.Property;
import com.natpryce.makeiteasy.PropertyLookup;
import com.sts.demo.domain.User;

public class UserMaker {

	public static final long DEFAULT_ID = 1L;
	public static final String DEFAULT_FIRSTNAME = "Stephen";
	public static final String DEFAULT_LASTNAME = "Hawking";
	public static final String DEFAULT_EMAIL = "stephen.hawking@gmail.com";
	public static final String DEFAULT_PASSWORD = "admin";
	
	public static final Property<User, Long> id = new Property<User, Long>();
	public static final Property<User, String> firstname = new Property<User, String>();
	public static final Property<User, String> lastname = new Property<User, String>();
	public static final Property<User, String> email = new Property<User, String>();
	public static final Property<User, String> password = new Property<User, String>();

	public static final Instantiator<User> User = new Instantiator<User>() {

		@Override
		public User instantiate(PropertyLookup<User> lookup) {
			User user = new User();

			user.setId(lookup.valueOf(id, DEFAULT_ID));
			user.setFirstname(lookup.valueOf(firstname, DEFAULT_FIRSTNAME));
			user.setLastname(lookup.valueOf(lastname, DEFAULT_LASTNAME));
			user.setEmail(lookup.valueOf(email, DEFAULT_EMAIL));
			user.setPassword(lookup.valueOf(password, DEFAULT_PASSWORD));

			return user;
		}
	};
}
