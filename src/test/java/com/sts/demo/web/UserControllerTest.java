package com.sts.demo.web;

import static com.natpryce.makeiteasy.MakeItEasy.a;
import static com.natpryce.makeiteasy.MakeItEasy.make;
import static com.sts.demo.builder.UserMaker.User;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.sts.demo.builder.UserMaker;
import com.sts.demo.web.UserController;
import com.sts.demo.domain.User;
import com.sts.demo.service.UserService;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

    private MockMvc mockMvc;
	
    @Mock
    private UserService userService;
    
    @InjectMocks
    private UserController userController;
    
    @Before
    public void setup() {
    	MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    public void showNewUserForm_ShouldRenderNewUserForm() throws Exception {
        mockMvc
        		.perform(get("/user?operation=newUser"))
		        .andExpect(status().isOk())
		        .andExpect(view().name("userForm"));

        verifyZeroInteractions(userService);
    }
    
    @Test
    public void listUsers_shouldAddUserListToModelAndRenderUserListView() throws Exception {

    	User defaltUser = make(a(User)); 
    	
    	List<User> userList = Arrays.asList(defaltUser);
    	
        when(userService.findAllUsers()).thenReturn(userList );

        mockMvc
        		.perform(get("/user?operation=userList"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("userList", userList))
                .andExpect(view().name("userList"))
                .andExpect(model().attribute("userList", hasSize(1)))
                .andExpect(model().attribute("userList", hasItem(
                        allOf(
                                hasProperty("id", is(equalTo(UserMaker.DEFAULT_ID))),
                                hasProperty("firstname", is(equalTo(UserMaker.DEFAULT_FIRSTNAME)))
                        )
                )));
                

        verify(userService, times(1)).findAllUsers();
        verifyNoMoreInteractions(userService);
    }
}
