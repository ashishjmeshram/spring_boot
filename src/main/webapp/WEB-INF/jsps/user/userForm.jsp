<%@ include file="../common/taglibs.jsp"%>

<script>
/* 
jQuery("#createUserForm").ajaxForm({
	beforeSubmit:function(){
		alert("beforeSubmit");
		jQuery("#processingRequestSpan").show();
		jQuery("#saveBtn").attr("disabled",true).addClass("disBtn");
		jQuery("#cancelBtn").attr("disabled",true).addClass("disBtn");	
		return true;
	}, 
	success:function(data){
		alert("success");
		jQuery("#processingRequestSpan").hide();
		jQuery("#saveBtn").attr("disabled",false).removeClass("disBtn");
		jQuery("#cancelBtn").attr("disabled",false).removeClass("disBtn");
	}

}); */

</script>

<form name="createUserForm" id="createUserForm" data-toggle="validator" class="form-horizontal" action="<c:url value='/user?operation=saveUser'/>" method="POST">

	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<input type="hidden" name="id" value="${ user.id }" />
	
	<div class="row">
		<div class="">
			<div class="panel panel-primary">
			
				<div class="panel-heading">
					<h3 class="panel-title">
					
					<c:choose>
						<c:when test="${ user eq null or user.id eq null or user.id =='' }">
							<fmt:message key="user.add"/>
						</c:when>
						<c:otherwise>
							<fmt:message key="user.edit"/>
						</c:otherwise>
					</c:choose>
					
					</h3>
				</div>
			
				<div class="panel-body">
				
					<div class="form-group">
						<label for="name" class="col-sm-2 control-label"><fmt:message key="user.firstname"/></label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="firstname" name="firstname" value="${ user.firstname }" placeholder="<fmt:message key="user.firstname"/>" required>
							<div class="help-block with-errors"></div>
						</div>
					</div>

					<div class="form-group">
						<label for="name" class="col-sm-2 control-label"><fmt:message key="user.lastname"/></label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="lastname" name="lastname" value="${ user.lastname }" placeholder="<fmt:message key="user.lastname"/>" required>
							<div class="help-block with-errors"></div>
						</div>
					</div>
					
					<div class="form-group">
						<label for="name" class="col-sm-2 control-label"><fmt:message key="user.email"/></label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="email" name="email" value="${ user.email }" placeholder="<fmt:message key="user.email"/>" required>
							<div class="help-block with-errors"></div>
						</div>
					</div>

					<div class="form-group">
						<label for="name" class="col-sm-2 control-label"><fmt:message key="user.password"/></label>
						<div class="col-sm-4">
							<input type="password" class="form-control" id="password" name="password" placeholder="<fmt:message key="user.password"/>" required>
							<div class="help-block with-errors"></div>
						</div>
					</div>

				</div>	
				<div class="panel-footer clearfix">
					<div class="pull-right">
						<button type="submit" class="btn btn-primary"><fmt:message key="save"/></button>
						<button type="button" class="btn btn-default" onclick="javascript:commonObj.goToURL('<c:url value='/user?operation=userList'/>');" ><fmt:message key="cancel"/></button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
