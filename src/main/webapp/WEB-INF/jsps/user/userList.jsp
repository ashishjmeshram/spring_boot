<%@ include file="../common/taglibs.jsp"%>

<script>
	$(document).ready(function(){
	    var grid = $("#user-grid").bootgrid({
	        selection: true,
	        multiSelect: true,
	        rowSelect: true,
	        keepSelection: true,
	        formatters: {
	        	"commands": function(column, row) {
	                return "<button type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\"" + row.id + "\"><span class=\"fa glyphicon glyphicon-pencil\"></span></button> " + 
	                    "<button type=\"button\" class=\"btn btn-xs btn-default command-delete\" data-row-id=\"" + row.id + "\"><span class=\"fa glyphicon glyphicon-trash\"></span></button>";
	            }
	        }
	    }).on("selected.rs.jquery.bootgrid", function(e, rows){
	        var rowIds = [];
	        for (var i = 0; i < rows.length; i++){
	            rowIds.push(rows[i].id);
	        }
	    }).on("deselected.rs.jquery.bootgrid", function(e, rows){
	        var rowIds = [];
	        for (var i = 0; i < rows.length; i++){
	            rowIds.push(rows[i].id);
	        }
	    }).on("loaded.rs.jquery.bootgrid", function(){
	    	
		    grid.find(".command-edit").on("click", function(e){
		    	var userId = $(this).data("row-id");
		    	var editURL = "<c:url value='/user?operation=editUser'/>&userId="+ userId;	
		    	commonObj.goToURL(editURL);
		    }).end().find(".command-delete").on("click", function(e){
		    	var userId = $(this).data("row-id");
		    	var deleteURL = "<c:url value='/user?operation=deleteUser'/>&userId="+ userId;	

		    	bootbox.confirm("Are you sure you want to delete this user?", function(result) {
		    		if(result){
				    	commonObj.goToURL(deleteURL); 
		    		}
		    	});
		    });
		});
	    
	});

</script>

<table id="user-grid" class="table table-condensed table-hover table-striped">
    <thead>
        <tr>
            <th data-column-id="id" data-type="numeric" data-identifier="true"><fmt:message key="user.id"/></th>
            <th data-column-id="firstname"><fmt:message key="user.firstname"/></th>
            <th data-column-id="lastname"><fmt:message key="user.lastname"/></th>
            <th data-column-id="email"><fmt:message key="user.email"/></th>
            <th data-column-id="commands" data-formatter="commands" data-sortable="false"><fmt:message key="actions"/></th>
        </tr>
    </thead>
    <tbody>
    	<c:forEach var="user" items="${userList}">
    		<tr>
    			<td>${user.id}</td>
    			<td>${user.firstname}</td>
    			<td>${user.lastname}</td>
    			<td>${user.email}</td>
    			<td></td>
    		</tr>
    	</c:forEach>
    </tbody>
</table>

<input class="btn btn-primary" type="button" value="<fmt:message key='user.addNew'/>" onclick="javascript:commonObj.goToURL('<c:url value='/user?operation=newUser'/>');">

	
	