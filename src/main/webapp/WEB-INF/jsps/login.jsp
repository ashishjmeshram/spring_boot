<%@ include file="common/taglibs.jsp"%>

<form class="form-horizontal" name="checkLogin" action="<c:url value='/checkLogin'/>" method="POST" data-toggle="validator">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">
							<fmt:message key="login" />
						</h3>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label"><fmt:message key="login.username" /></label>
							<div class="col-sm-10">
								<input type="email" class="form-control" name="username" id="username" placeholder="<fmt:message key="login.username"/>" required>
							</div>
						</div>
						<div class="form-group">
							<label for="password" class="col-sm-2 control-label"><fmt:message key="login.password" /></label>
							<div class="col-sm-10">
								<input type="password" class="form-control" name="password" id="password" placeholder="<fmt:message key="login.password"/>" required>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<button type="submit" class="btn btn-primary" style="float: right" >
			  				<fmt:message key="login.signin"/>
						</button>
						<div class="clearfix"></div>
					</div>
			</div>
			<c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION and param.login_error eq 1}">
				<div class="alert alert-warning" role="alert">
				    <font color="red"><strong><fmt:message key="login.error"/></strong></font>
				</div>
			</c:if>
		</div>
	</div>
</form>
