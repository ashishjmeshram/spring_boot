<!DOCTYPE html>

<%@ include file="../common/taglibs.jsp"%>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Simple Gradle Application</title>

	<!-- CSS files go below -->
    <link rel="stylesheet" href="<c:out value='static/css/bootstrap.min.css'/>" >
	<link rel="stylesheet" href="<c:out value='static/css/layout.css'/>" />
	<link rel="stylesheet" href="<c:out value='static/css/jquery.bootgrid.min.css'/>" />
	
	<!-- Javascript files go below  -->
	<script type="text/javascript" src="<c:out value='static/js/jquery-1.11.3.min.js'/>"></script>
    <script type="text/javascript" src="<c:out value='static/js/bootstrap.min.js' />"></script>
	<script type="text/javascript" src="<c:out value='static/js/jquery.bootgrid.min.js'/>"></script>
	
	<script type="text/javascript" src="<c:out value='static/js/validator.min.js'/>"></script>
	<script type="text/javascript" src="<c:out value='static/js/bootbox.min.js'/>"></script>
	<script type="text/javascript" src="<c:out value='static/js/jquery.i18n.properties.js'/>"></script>
	<script type="text/javascript" src="<c:out value='static/js/jquery.form.js'/>"></script>
	
	<script type="text/javascript" src="<c:out value='static/js/common.js'/>"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
    	var i18n;	
    	var commonObj = new common();
    	jQuery(document).ready(function(){
    		jQuery.i18n.properties({
			    name:['messages'], 
			    path:'<c:url value="/i18n/"/>',
			    language:'en', 
			    mode:'map',
			    callback: function() {
					i18n = 	jQuery.i18n.prop;
			    }
			});
    	});
    </script>
  </head>
  <body>
	<div class="container-fluid">
		<tiles:insertAttribute name="header" />
		<div class="row">
			<div class="col-lg-3">
				<tiles:insertAttribute name="menu" />
			</div>
			<div class="col-lg-9">
				<tiles:insertAttribute name="body" />
			</div>
		</div>
		<tiles:insertAttribute name="footer" />
	</div>
  </body>
</html>