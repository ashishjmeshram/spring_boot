package com.sts.demo.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.sts.demo.domain.User;

public class SecurityUser extends User implements UserDetails {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 8009845970491889000L;

	public SecurityUser(User user) {
		if (user != null) {
			this.setId(user.getId());
			this.setFirstname(user.getFirstname());
			this.setLastname(user.getLastname());
			this.setEmail(user.getEmail());
			this.setPassword(user.getPassword());
		}
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getUsername() {
		return super.getEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
