package com.sts.demo.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sts.demo.domain.User;
import com.sts.demo.service.UserService;

@Service("userDetailsService")
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

	private static final Logger log = LoggerFactory.getLogger(UserDetailsService.class);

	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(final String userName) throws UsernameNotFoundException, DataAccessException {
		log.debug("Authenticating : {}", userName);

		User user = null;

		try {
			user = userService.findUserByEmail(userName);

			if (user == null) {
				log.debug("user with the username [{}]  NOT FOUND", userName);
				throw new UsernameNotFoundException("Username not found.");
			}

			log.debug("User with the username [{}] FOUND ", userName);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new SecurityUser(user);
	}

}
