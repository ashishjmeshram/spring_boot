package com.sts.demo.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Profile;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Profile(ProdEnvironment.NAME)
public @interface ProdEnvironment {
	String NAME = Constants.SPRING_PROFILE_PRODUCTION;
}
