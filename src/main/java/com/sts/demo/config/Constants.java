package com.sts.demo.config;

/**
 * Application constants.
 */
public final class Constants {

    // Spring profile for development, production and test
    public static final String SPRING_PROFILE_DEVELOPMENT = "dev";
    public static final String SPRING_PROFILE_PRODUCTION = "prod";
    public static final String SPRING_PROFILE_TEST = "test";

    public static final String SPRING_PROFILE_DEFAULT = SPRING_PROFILE_PRODUCTION;

    private Constants() {
    }
}
