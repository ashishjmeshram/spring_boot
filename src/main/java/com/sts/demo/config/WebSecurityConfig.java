package com.sts.demo.config;

import org.jasypt.digest.PooledStringDigester;
import org.jasypt.salt.RandomSaltGenerator;
import org.jasypt.springsecurity3.authentication.encoding.PasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;

	@Override
	@Autowired
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	@Bean
	public PasswordEncoder passwordEncoder(){
		PasswordEncoder passwordEncoder = new PasswordEncoder();
		passwordEncoder.setStringDigester(stringDigester());
		return passwordEncoder;
	}
	
	@Bean
	public PooledStringDigester stringDigester() {
		PooledStringDigester psd = new PooledStringDigester();

		psd.setPoolSize(2);
		psd.setAlgorithm("SHA-256");
		psd.setIterations(1000);
		psd.setSaltSizeBytes(16);
		psd.setSaltGenerator(randomSaltGenerator());

		return psd;
	}

	@Bean
	public RandomSaltGenerator randomSaltGenerator() {
		RandomSaltGenerator randomSaltGenerator = new RandomSaltGenerator();
		return randomSaltGenerator;
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring()
				.antMatchers("/static/**")
				.antMatchers("/i18n/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
            .csrf()
        .and()
			.exceptionHandling()
			.accessDeniedPage("/accessDenied")
		.and()
			.formLogin()
            .loginPage("/login")
            .loginProcessingUrl("/checkLogin")
            .defaultSuccessUrl("/home")
            .failureUrl("/login?login_error=1")
            .usernameParameter("username")
            .passwordParameter("password")
            .permitAll()
        .and()
			.logout()
            .logoutUrl("/logout")
            .logoutSuccessUrl("/login?isLoggedOut=1")
            .deleteCookies("JSESSIONID")
            .invalidateHttpSession(true)
            .permitAll()
        .and()
            .authorizeRequests()
            .antMatchers("/login**").permitAll()
            .antMatchers("/checkLogin**").permitAll()
            .anyRequest()
            .authenticated()
        .and()
            .headers()
            .frameOptions()
            .disable()
        .and()
			.sessionManagement()
            .invalidSessionUrl("/login?time=1")
            .maximumSessions(1);
	}

}
