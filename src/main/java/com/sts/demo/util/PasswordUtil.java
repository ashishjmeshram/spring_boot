package com.sts.demo.util;

import org.jasypt.digest.PooledStringDigester;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("passwordUtil")
public class PasswordUtil {

	@Autowired
	private PooledStringDigester _stringDigester;

	public String encryptUserPassword(String originalPassword) {
		String encryptedPassword = _stringDigester.digest(originalPassword);
		return encryptedPassword;
	}
}
