package com.sts.demo.util;

public class WebUtil {
	
	public static String createRedirectViewPath(String requestMapping) {
	    StringBuilder redirectViewPath = new StringBuilder();
	    redirectViewPath.append("redirect:");
	    redirectViewPath.append(requestMapping);
	    return redirectViewPath.toString();
	}

}
