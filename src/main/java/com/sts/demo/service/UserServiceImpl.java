package com.sts.demo.service;

import java.util.Date;
import java.util.List;

import com.sts.demo.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sts.demo.domain.User;
import com.sts.demo.util.PasswordUtil;
	
@Service("userService")
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

	private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordUtil passwordUtil;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackForClassName = {"java.lang.Exception" })
	public void saveUser(User user, Long loggedInUserId) {
		log.debug("Saving an user with the information: {}", user);
		
		boolean isNew = user.getId() == null || user.getId() == 0;
		
		if(isNew){
			user.setPassword(passwordUtil.encryptUserPassword(user.getPassword()));
			user.setCreatedBy(loggedInUserId);
			user.setCreatedDate(new Date());
			userRepository.save(user);
		} else {
			
			User dbUser = userRepository.findOne(user.getId());
			
			dbUser.setFirstname(user.getFirstname()); 
			dbUser.setLastname(user.getLastname()); 
			dbUser.setEmail(user.getEmail()); 
			
			dbUser.setUpdatedBy(loggedInUserId);
			dbUser.setUpdatedDate(new Date());

			if(!StringUtils.isEmpty(user.getPassword())){
				dbUser.setPassword(passwordUtil.encryptUserPassword(user.getPassword()));
			}
			userRepository.save(dbUser);
		}
		
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackForClassName = {"java.lang.Exception" })
	public void deleteUser(Long userId) {
		log.debug("Deleting an user entry with the user id: {}", userId);
		User user = userRepository.findOne(userId);
		userRepository.delete(user);
	}

	@Override
	public User findUserById(Long id) {
		log.debug("Finding an user entry with the user id: {}", id);
		return userRepository.findOne(id);
	}

	@Override
	public List<User> findAllUsers() {
		log.debug("Finding all user entries");
		return (List<User>) userRepository.findAll();
	}

	@Override
	public User findUserByEmail(String email) {
		log.debug("Finding an user entry with the email: {}", email);
		return userRepository.findByEmail(email);
	}

}
