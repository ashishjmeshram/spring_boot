package com.sts.demo.service;

import java.util.List;

import com.sts.demo.domain.User;

public interface UserService {

	void saveUser(User user, Long loggedInUserId);

	void deleteUser(Long userId);

	User findUserById(Long id);

	List<User> findAllUsers();
	
	User findUserByEmail(String email);
}
