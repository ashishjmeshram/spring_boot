package com.sts.demo.web;

import com.sts.demo.security.SecurityUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sts.demo.domain.User;
import com.sts.demo.service.UserService;
import com.sts.demo.util.WebUtil;

@Controller
@RequestMapping(value = { "/user"})
public class UserController {

	public static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@RequestMapping(params = "operation=userList")
	public String userList(Model model) {
		model.addAttribute("userList", userService.findAllUsers());
		return "userList";
	}

	@RequestMapping(params = "operation=newUser")
	public String newUser(Model model) {
		return "userForm";
	}

	@RequestMapping(params = "operation=editUser")
	public String editUser(Long userId, Model model) {
		model.addAttribute("user", userService.findUserById(userId));
		return "userForm";
	}

	@RequestMapping(params = "operation=saveUser")
	public String saveUser(User user, Model model) {
		userService.saveUser(user, SecurityUtil.getCurrentUser().getId());
		return WebUtil.createRedirectViewPath("/user?operation=userList");
	}

	@RequestMapping(params = "operation=deleteUser")
	public String deleteUser(Long userId, Model model) {
		userService.deleteUser(userId);
		return WebUtil.createRedirectViewPath("/user?operation=userList");
	}

}
