package com.sts.demo.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = { HomeController.HOME_CONTROLLER_URL })
public class HomeController {

	static final String HOME_CONTROLLER_URL = "/home";

	public static final Logger log = LoggerFactory.getLogger(HomeController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String home() {
		log.debug("Showing home page.");
		return "home";
	}
}
