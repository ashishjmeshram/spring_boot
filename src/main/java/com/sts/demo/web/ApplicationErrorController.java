package com.sts.demo.web;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ApplicationErrorController implements ErrorController {

	private static final String PATH = "/error";

	@RequestMapping(value = "/pageNotFound", method = { RequestMethod.GET, RequestMethod.POST })
	public String pageNotFound() {
		return "pageNotFound";
	}

	@RequestMapping(value = PATH)
	public String error() {
		return "error";
	}

	@RequestMapping(value = "/accessDenied", method = { RequestMethod.GET, RequestMethod.POST })
	public String accessDenied() {
		return "accessDenied";
	}

	@Override
	public String getErrorPath() {
		return PATH;
	}
}
